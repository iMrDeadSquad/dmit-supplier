package nl.fontys.supplier.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
//import io.github.cdimascio.dotenv.Dotenv;
import nl.fontys.supplier.models.Supplier;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class SupplierSendService {

    //private static final Dotenv env = Dotenv.load();

    private static final String EXCHANGE_NAME = "supplier_exchange";

    private static Channel channel;

    static {
        try {
            if (channel == null) {
                channel = getFactory().newConnection().createChannel();
            }
        } catch (IOException e) {
            e.printStackTrace(); //NOSONAR
        } catch (TimeoutException e) {
            e.printStackTrace(); //NOSONAR
        }
    }

//    private static ConnectionFactory getFactory() {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//        factory.setUsername("guest");
//        factory.setPassword("guest");
//        return factory;
//    }


    private static ConnectionFactory getFactory() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        factory.setUsername("admin");
        factory.setPassword("root");
        return factory;
    }

    public static void publishNewSupplier(Supplier supplier) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String json = new Gson().toJson(supplier);
        channel.basicPublish(EXCHANGE_NAME, "supplier.add", null, json.getBytes(StandardCharsets.UTF_8));
    }

    public static void publishDeleteSupplier(Supplier supplier) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String json = new Gson().toJson(supplier);
        channel.basicPublish(EXCHANGE_NAME, "supplier.delete", null, json.getBytes(StandardCharsets.UTF_8));
    }

    public static void publishUpdateSupplier(Supplier supplier) throws IOException {
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String json = new Gson().toJson(supplier);
        channel.basicPublish(EXCHANGE_NAME, "supplier.update", null, json.getBytes(StandardCharsets.UTF_8));
    }

    private static void publish(String queue, String message) {
        try {

            // makes sure that the messages are saved for when RabbitMQ stops
            boolean durable = true;
            channel.queueDeclare(queue, durable, false, false, null);
            channel.basicPublish("", queue, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());

        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
