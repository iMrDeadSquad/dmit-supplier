package nl.fontys.supplier.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Supplier{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "supplier_uuid")
    private String uuid;

    @NotBlank
    private String companyName;

    private String contact;

    private String street;

    private Integer houseNumber;

    private String addition;

    @Size(max = 6)
    private String postalCode;

    private String place;

    @Email
    private String email;

    private Long phoneNumber;


    public Supplier(String companyName, String contact, String street, Integer houseNumber, String addition, String postalCode, String place, String email, Long phoneNumber) {
        this.uuid = UUID.randomUUID().toString();
        this.companyName = companyName;
        this.contact = contact;
        this.street = street;
        this.houseNumber = houseNumber;
        this.addition = addition;
        this.postalCode = postalCode;
        this.place = place;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Supplier(String uuid,String companyName, String contact, String street, Integer houseNumber, String addition, String postalCode, String place, String email, Long phoneNumber) {
        this.uuid = uuid;
        this.companyName = companyName;
        this.contact = contact;
        this.street = street;
        this.houseNumber = houseNumber;
        this.addition = addition;
        this.postalCode = postalCode;
        this.place = place;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
}

