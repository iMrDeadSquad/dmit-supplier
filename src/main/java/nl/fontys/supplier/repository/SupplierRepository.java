package nl.fontys.supplier.repository;

import nl.fontys.supplier.models.Supplier;
import nl.fontys.supplier.rabbitmq.SupplierSendService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SupplierRepository extends JpaRepository<Supplier, Long>{
    //List<Customer> findByActive(boolean active);
    //List<Customer> findByCompanyName(String companyName);

    @Query("select c from Supplier c " +
            "where lower(c.companyName) like lower(concat('%', :searchTerm, '%')) ")
    List<Supplier> findByCompanyName(@Param("searchTerm") String companyName);

//    @Query("select c from Supplier c " +
//            "where lower(c.status) like lower(concat('%', :status, '%')) ")
//    List<Supplier> findByActive(@Param("status") Enum status);

    Supplier findByUuid (String id);

    void deleteById(Optional<Supplier> supplier);

//    Supplier findById (long id);

   }
