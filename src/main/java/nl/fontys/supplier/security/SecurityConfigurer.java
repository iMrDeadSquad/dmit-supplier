package nl.fontys.supplier.security;

import nl.fontys.supplier.config.filters.JwtFilter;
import nl.fontys.supplier.models.Jwt;
import nl.fontys.supplier.redis.JwtRedis;
//import nl.fontys.supplier.security.jwt.AuthEntryPointJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(
//        // securedEnabled = true,
//        // jsr250Enabled = true,
//        prePostEnabled = true)
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {
	@Autowired
	private JwtRedis jwtRedis;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
			.exceptionHandling();
//				.and()
//			.authorizeRequests().antMatchers("/api/suppliers/**").permitAll()
//			.anyRequest().authenticated();
//		http.addFilterAfter(
//				new JwtFilter(jwtRedis), BasicAuthenticationFilter.class);
	}
}