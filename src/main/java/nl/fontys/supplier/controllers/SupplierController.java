package nl.fontys.supplier.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import nl.fontys.supplier.models.Supplier;
import nl.fontys.supplier.rabbitmq.SupplierSendService;
import nl.fontys.supplier.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class SupplierController {

    @Autowired
    SupplierRepository supplierRepository;

    @GetMapping("/suppliers")
    public ResponseEntity<List<Supplier>> getAllSuppliers(@RequestParam(required = false) String companyName) {
        try {
            List<Supplier> suppliers = new ArrayList<Supplier>();

            if (companyName == null || companyName.equals(""))
                supplierRepository.findAll().forEach(suppliers::add);
            else
                supplierRepository.findByCompanyName(companyName).forEach(suppliers::add);

            if (suppliers.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(suppliers, HttpStatus.OK);
        }   catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/suppliers/{id}")
    public ResponseEntity<Supplier> getSupplierById(@PathVariable("id") String id) {
        Supplier supplierData = supplierRepository.findByUuid(id);

        if (supplierData != null) {
            return new ResponseEntity<>(supplierData, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/suppliers")
    public ResponseEntity<Supplier> createSupplier(@RequestBody Supplier supplier) { //NOSONAR
        try {
            Supplier _supplier = supplierRepository
                    .save(new Supplier(supplier.getCompanyName(), supplier.getContact(), supplier.getStreet(), supplier.getHouseNumber(), supplier.getAddition(), supplier.getPostalCode(), supplier.getPlace(), supplier.getEmail(), supplier.getPhoneNumber()));
            SupplierSendService.publishNewSupplier(_supplier);
            return new ResponseEntity<>(_supplier, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/suppliers/{id}")
    public ResponseEntity<Supplier> updateSupplier(@PathVariable("id") String id, @RequestBody Supplier supplier) throws IOException { //NOSONAR
        Supplier supplierData = supplierRepository.findByUuid(id);
          if (supplierData != null) {
                Supplier _supplier = supplierData;
                _supplier.setCompanyName(supplier.getCompanyName());
                _supplier.setContact(supplier.getContact());
                _supplier.setStreet(supplier.getStreet());
                _supplier.setHouseNumber(supplier.getHouseNumber());
                _supplier.setAddition(supplier.getAddition());
                _supplier.setPostalCode(supplier.getPostalCode());
                _supplier.setPlace(supplier.getPlace());
                _supplier.setEmail(supplier.getEmail());
                _supplier.setPhoneNumber(supplier.getPhoneNumber());
//            _supplier.setNotesList(supplier.getNotesList());
                SupplierSendService.publishUpdateSupplier(_supplier);
                supplierRepository.save(_supplier);
                return new ResponseEntity<>(_supplier, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
    }

//    @DeleteMapping("/suppliers/{id}")
//    public ResponseEntity<HttpStatus> deleteSupplier(@PathVariable("id") String uuid) {
//        try {
//            Supplier supplier = supplierRepository.findByUuid(uuid);
//            SupplierSendService.publishDeleteSupplier(supplier);
//            supplierRepository.deleteById(supplier.getId());
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        } catch (Exception e) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//    @DeleteMapping("/suppliers/{id}")
//    public ResponseEntity<HttpStatus> deleteSupplier(@PathVariable("id") String id) {
//        try {
//            Optional<Supplier> supplier = supplierRepository.findById(id);
//            if (supplier.isPresent()){
//                Supplier supplierRMQ = supplierRepository.findByUuid(supplier.get().getUuid());
//                SupplierSendService.publishDeleteSupplier(supplierRMQ);
//                supplierRepository.deleteById(supplierRMQ.getId());
//                System.out.println("Supplier removed");
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//            }
//            else {
//                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//            }
//        } catch (Exception e) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @DeleteMapping("/suppliers/{id}")
    public ResponseEntity<HttpStatus> deleteSupplier(@PathVariable("id") String id) {
        try {
            Supplier supplier = supplierRepository.findByUuid(id);
            if (supplier != null){
                supplierRepository.deleteById(supplier.getId());
                SupplierSendService.publishDeleteSupplier(supplier);
                System.out.println("Supplier removed");
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/suppliers")
    public ResponseEntity<HttpStatus> deleteAllSupplier() {
        try {
            supplierRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @GetMapping("/suppliers/active")
//    public ResponseEntity<List<Supplier>> findByActive() {
//        try {
//            List<Supplier> suppliers = supplierRepository.findByActive(true);
//
//            if (suppliers.isEmpty()) {
//                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//            }
//            return new ResponseEntity<>(suppliers, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
}
