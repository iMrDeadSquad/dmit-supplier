package nl.fontys.supplier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class SupplierApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(SupplierApplication.class, args);
				//System.out.println("Supplier service ready.");
	}

}
