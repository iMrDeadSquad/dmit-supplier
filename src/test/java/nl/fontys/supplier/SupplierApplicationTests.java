package nl.fontys.supplier;

import nl.fontys.supplier.controllers.SupplierController;
import nl.fontys.supplier.models.Supplier;
import nl.fontys.supplier.repository.SupplierRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SupplierApplicationTests {

	@Mock
	private SupplierRepository supplierRepository;

	@InjectMocks
	private SupplierController supplierController;

	private Supplier supplier1Mock;
	private Supplier supplier2Mock;
	private Supplier supplier3Mock;

	private List<Supplier> mockSuppliers;


	@BeforeEach
	private void init() {
		supplier1Mock = new Supplier("uuid1","fontys", "Pieter", "Van Schoolstraat", 9, "", "5050TZ", "Tilburg", "pieter@fontys.nl", 543570862L);
		supplier2Mock = new Supplier("uuid2","avans", "Pieter", "Van Schoolstraat", 9, "", "5050TZ", "Tilburg", "pieter@fontys.nl", 543570862L);
		supplier3Mock = new Supplier("uuid3","jumbo", "Pieter", "Van Schoolstraat", 9, "", "5050TZ", "Tilburg", "pieter@fontys.nl", 543570862L);
		supplierRepository.save(supplier1Mock);
		supplierRepository.save(supplier2Mock);
		supplierRepository.save(supplier3Mock);

	}

	@Test
	public void should_find_no_suppliers_if_repository_is_empty() {
		Iterable<Supplier> suppliers = supplierRepository.findAll();
		assertThat(suppliers).isEmpty();
	}

	@Test
	public void get_all_suppliers() {

		List<Supplier> suppliers = new ArrayList<>();
		suppliers.add(supplier1Mock);
		suppliers.add(supplier2Mock);
		suppliers.add(supplier3Mock);
		given(supplierRepository.findAll()).willReturn(suppliers);

		ResponseEntity<List<Supplier>> suppliers1 = supplierController.getAllSuppliers("");

		assertNotNull(suppliers1);
		assertTrue(suppliers1.hasBody());

		assertEquals(suppliers1.getBody().get(0),supplier1Mock);
		assertEquals(suppliers1.getBody().get(1),supplier2Mock);
		assertNotEquals(suppliers1.getBody().get(1), supplier3Mock);

		assertEquals(suppliers1.getStatusCode(), HttpStatus.OK);

	}

	@Test
	void get_all_supplier_when_empty() {

		List<Supplier> suppliers = new ArrayList<>();
		given(supplierRepository.findAll()).willReturn(suppliers);

		ResponseEntity<List<Supplier>> suppliers1 = supplierController.getAllSuppliers("");

		assertFalse(suppliers1.hasBody());
		assertEquals(suppliers1.getStatusCode(), HttpStatus.NO_CONTENT);
	}

	@Test
	public void should_find_supplier_by_id() {
		String id = "uuid1";

		given(supplierRepository.findByUuid(id)).willReturn(supplier1Mock);

		ResponseEntity<Supplier> supplier = supplierController.getSupplierById(id);

		assertEquals(supplier.getBody(),supplier1Mock);
		assertEquals(supplier.getStatusCode(), HttpStatus.OK);
	}

	@Test
	public void should_find_supplier_by_companyname_containing_string() {

		List<Supplier> suppliersMock = new ArrayList<>();
		suppliersMock.add(supplier3Mock);
		String companyName = "umb";
		given(supplierRepository.findByCompanyName(companyName)).willReturn(suppliersMock);

		ResponseEntity<List<Supplier>> suppliers1 = supplierController.getAllSuppliers(companyName);

		assertNotNull(suppliers1);
		assertTrue(suppliers1.hasBody());

		assertEquals(suppliers1.getBody().get(0),supplier3Mock);

		assertEquals(suppliers1.getStatusCode(), HttpStatus.OK);
	}

//	@Test
//	public void should_update_supplier_by_id() throws IOException {
//		String id = "uuid2";
//		Supplier updateSup = new Supplier("uuid2","avans new", "Pieter new", "Van Schoolstraat", 9, "", "5050TZ", "Tilburg", "pieter@fontys.nl", 543570862L);
//		given(supplierRepository.findByUuid(id)).willReturn(supplier2Mock);
//
//		ResponseEntity<Supplier> suppliers1 = supplierController.updateSupplier(id,updateSup);
//
//		assertThat(supplier2Mock.getId()).isEqualTo(suppliers1.getBody().getId());
//		assertThat(supplier2Mock.getCompanyName()).isEqualTo(suppliers1.getBody().getCompanyName());
//		assertThat(supplier2Mock.getContact()).isEqualTo(suppliers1.getBody().getContact());
//		assertThat(supplier2Mock.getPlace()).isEqualTo(suppliers1.getBody().getPlace());
//
//		assertEquals(suppliers1.getStatusCode(), HttpStatus.OK);
//	}

//	@Test
//	public void should_create_supplier() throws InterruptedException {
//		Supplier newSupplier = new Supplier("avans 2", "Pieter 2", "Van Schoolstraat", 9, "", "5050TZ", "Tilburg", "pieter@fontys.nl", 543570862L);
//		given(supplierRepository.save(newSupplier));
//		final ResponseEntity<Supplier>[] suppliers1 = new ResponseEntity[]{null};
//		when(mock.load("a")).thenAnswer(new Answer<ResponseEntity<Supplier>>() {
//			@Override
//			public ResponseEntity<Supplier> answer(InvocationOnMock invocation) throws InterruptedException {
//				suppliers1[0] = supplierController.createSupplier(newSupplier);
//				Thread.sleep(5000);
//				return suppliers1[0];
//			}
//		});
//
//		assertEquals(HttpStatus.CREATED, suppliers1[0].getStatusCode());
//	}

//	@Test
//	public void should_delete_supplier_by_id() {
//		Long id = Long.valueOf(2);
//		String uuid = "uuid2";
//		Supplier deleteSupplier = new Supplier("uuid2","avans 2", "Pieter 2", "Van Schoolstraat", 9, "", "5050TZ", "Tilburg", "pieter@fontys.nl", 543570862L);
//		given(supplierRepository.findByUuid(uuid)).willReturn(deleteSupplier);
//
//		ResponseEntity<HttpStatus> httpStatus = supplierController.deleteSupplier(uuid);
//
//		assertEquals(HttpStatus.NO_CONTENT, httpStatus.getStatusCode());
//	}
}